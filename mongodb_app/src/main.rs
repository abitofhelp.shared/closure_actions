use std::string::ToString;

use bson::oid::ObjectId;
use uuid::Uuid;

use crate::entity_error::{BoxError, EntityError};

mod entity_error;
mod error;

#[derive(Debug)]
pub enum DataId {
    InMemory(Uuid),
    MongoDb(ObjectId),
}

impl DataId {}

#[tokio::main]
async fn main() {
    let uuid = "142c458f-2728-4961-b4c5-6514c7491691";
    let oid = "6507fdd16c27fffd69308982";

    // Parse an existing inmemory uuid.
    let result_uuid = make_id(Some(uuid), parse_uuid, generate_uuid).unwrap();
    println!("Parsed UUID '{}' to '{:?}'", uuid, result_uuid);

    let result_objectid = make_id(Some(oid), parse_objectid, generate_objectid).unwrap();
    println!("Parsed ObjectId '{}' to '{:?}'", oid, result_objectid);
}

// Create an instance of a data storage id that is specific for an inmemory store or a MongoDb database.
// id: The value of a unique id for a project in storage as a string slice.
// parser: A closure that parses the UUID or ObjectId.
// generator: A closure that generates a new UUID or ObjectId.
// returns: the data store specific id.
fn make_id(
    id: Option<&str>,
    parser: fn(id: &str) -> Result<DataId, EntityError>,
    generator: fn() -> DataId,
) -> Result<DataId, EntityError> {
    return if let Some(id) = id {
        match parser(id) {
            Ok(did) => Ok(did),
            Err(e) => Err(e),
        }
    } else {
        // No id provided, so we will make one.
        Ok(generator())
    };
}

fn parse_uuid(id: &str) -> Result<DataId, EntityError> {
    match Uuid::parse_str(id) {
        Ok(uid) => Ok(DataId::InMemory(uid)),
        Err(e) => Err(EntityError::Application {
            message: "failed to parse the uuid".to_string(),
            err: Some(BoxError::try_from(e).unwrap()),
        }),
    }
}

fn parse_objectid(id: &str) -> Result<DataId, EntityError> {
    match ObjectId::parse_str(id) {
        Ok(oid) => Ok(DataId::MongoDb(oid)),
        Err(e) => Err(EntityError::Application {
            message: "failed to parse the object id".to_string(),
            err: Some(BoxError::try_from(e).unwrap()),
        }),
    }
}

fn generate_uuid() -> DataId {
    DataId::InMemory(Uuid::new_v4())
}

fn generate_objectid() -> DataId {
    DataId::MongoDb(ObjectId::new())
}
